#Series for functions to take a file of epithelium or mesenchymal intensity (or thickness) profiles (as out put from "Expression profiles.ijm" ImageJ macros) and convert them into a kymograph of gene expression (ie staining intensity) against time

#Read in output ImageJ text file using 'input_profile<-read.table("~/Documents/Profiles/Profiles.txt",sep="\t",header=T,stringsAsFactor=F)'
#Sets of images recorded in different files can simply be combined using 'rbind' function


#Function 'profile' to extract profile from 'input_profile' for a given specimen (specimen number of the form "1-23 4a" as in ImageJ output file), type (epithelium ("E"), meneschyme ("M") or thickness ("T") and gene (eg "Shh")
#Unless specified using a 'slices' vector of desired section numbers (as in ImageJ output file), all sections recoreded in 'input_profile' for a given specimen will be used
#Intensity profiles for each section will be registered by the landmark at the desired position (default 1000 pixels)
#Function outputs a matrix of two columns: first the mean intensity across all sections at each pixel along profile (these are based on the raw intensities and not normalised as all sections for a particular gene for each specimen are in situed on the same slide and should therefore be comparable), and second the standard deviation of intensity values between the different sections at each pixel postion along the profiles

profile<-function(input_profile,specimen,type,gene,slices=NA,landmark=1000,plot=TRUE,...){
	input_profile<-input_profile[(input_profile[,2]==specimen),]
	input_profile<-input_profile[(input_profile[,5]==type),]
	if(type!="T"){
		input_profile<-input_profile[(input_profile[,1]==gene),]		
	}
	if(!is.na(slices)){
		input_profile<-input_profile[(input_profile[,3]%in%slices),]
	}
	profiles<-list()
	num<-length(input_profile[,1])
	reg<-numeric()
	length<-numeric()
	land<-input_profile[,4] #extract registration point into array
	for(i in 1:num){ #get array "length" of profile lengths
		profile_array<-strsplit(input_profile[i,6]," ")[[1]]
		profile_array[profile_array=="NA"]<-NA
		profiles[[i]]<-as.numeric(profile_array)
		length[i]<-length(profiles[[i]])
	}
	range<-length-land #array of lengths of profiles beyond landmark
	max_range<-max(range) #longest range beyond landmark
	upper_lim<-max_range+landmark #maximum length
	m<-rep(NA,upper_lim*num)
	M<-matrix(m,ncol=num) #make a matrix of all the values with NA in blanks
	profile_mean<-numeric(upper_lim)
	profile_sd<-numeric(upper_lim)
	for(i in 1:upper_lim){
		for(j in 1:num){
			pos<-land[j]+i-landmark
			if(pos>0){
				M[i,j]<-profiles[[j]][pos]	
			}		
		}
		reg<-M[i,]
		data_reg<-reg[!is.na(reg)]
		if(((length(data_reg))/(length(reg)))>=(3/4)){
			profile_mean[i]<-mean(data_reg)
			profile_sd[i]<-sd(data_reg)	
		}
		else{
			profile_mean[i]<-NA
			profile_sd[i]<-NA		
		}
	}
	if(length(profile_sd[is.na(profile_sd)])==length(profile_sd)){
		upper_bound<-profile_mean
		lower_bound<-profile_mean	
	}
	else{
		upper_bound<-profile_mean+profile_sd
		lower_bound<-profile_mean-profile_sd	
	}
	max<-max(upper_bound[!is.na(upper_bound)])
	min<-min(lower_bound[!is.na(lower_bound)])
	if(plot==TRUE){
		if(type=="T"){
			plot(profile_mean,type="l",ylim=c(min,max),lwd=2,xlab=NULL,ylab=NULL,...)
			lines(upper_bound,lty=3)
			lines(lower_bound,lty=3)
		}
		else{
			plot(profile_mean,type="l",ylim=c(max,min),lwd=2,xlab=NULL,ylab=NULL,...)
			lines(upper_bound,lty=3)
			lines(lower_bound,lty=3)
		}
	}
	array<-cbind(profile_mean,profile_sd)
	array
}


#Function 'smooth'  smoothes all profile arrays within 'input_profile', by taking an mean of positions in the array ± 'width' around each position in the array 

smooth<-function(input_profile,width=25){
	num<-length(input_profile[,1])
	#input_profile[,4]<-input_profile[,4]-width
	for(i in 1:num){
		profiles<-as.numeric(strsplit(input_profile[i,6]," ")[[1]])
		len<-length(profiles)
		smoothing<-rep(NA,len)
		for(j in (width+1):(len-width)){
			smoothing[j]<-mean(profiles[seq((j-width),(j+width))])
		}
		input_profile[i,6]<-paste(smoothing,collapse=" ")
	}
	input_profile
}


#Function 'expression_map' extracts profiles from 'input_profile' for given type (E, M or T) and gene, for specific specimens as identified in the 'specimens' vector (of the form "1-23 4a"), and outputs a matrix where each column is an expression/thickness profile 
#Profiles are aligned at the landmark (eg ruga 3) at the specified row as specified in 'landmark', with columns a total length as sepcified in 'length', with columns in the sequence specified in 'specimens' vector
#Section slices for each specimen can be specified using the 'profile_slices', otherwise if using default NA all slices for  specimen of the given gene and type are used
#If 'specimens' vector is ranked by increasing length, expression_map can be used to produce a preliminary kymograph (although the 'time' axis is not clibrated), and this can be plotted using 'plot'
#Use 'smooth' to smooth kymograph, by averaging every term in the matrix by the previous an successive term in each column 

expression_map<-function(input_profile,type,gene,specimens=NA,profile_slices=NA,landmark=1000,length=2000,smooth=TRUE,plot=TRUE){
	number<-length(specimens)
	m<-rep(NA,length*number)
	M<-matrix(m,ncol=number)
	for(i in 1:number){
		if(is.na(profile_slices)==TRUE){
			slices<-NA			
		}
		else{
			slices<-profile_slices[,1][[i]]		
		}
		array<-profile(input_profile,specimens[i],type,gene,slices=slices,landmark=landmark,plot=FALSE)
		array<-array[,1]
		max<-max(array[!is.na(array)])
		min<-min(array[!is.na(array)])
		for(j in 1:length){
			M[j,i]<-(array[j]-min)/(max-min)#M[j,i]<-array[j]##M[j,i]<-1-(255-array[j])/(255-min)
		}		
	}
	if(smooth==TRUE){
		M<-smoothing(M)
	}
	if(type=="T"){
		range<-grey.colors(100,1,0)
	}
	else{
		range<-grey.colors(100,0,1)
	}
	if(plot==TRUE){
		image(t(M),col=range,axes=FALSE)
	}
	M
}


#Funtion 'smoothing' smooths output matrix from 'expression_map' (see above)

smoothing<-function(M){
	length<-length(M[,1])
	number<-length(M[1,])
	number2<-number-2
	s<-rep(NA,length*number2)
	S<-matrix(s,ncol=number2)
	for(j in 2:(number-1)){
		for(i in 1:(length)){
			S[i,j-1]<-mean(c(M[i,j-1],M[i,j],M[i,j+1]))
		}
	}
	S
}


#Function 'rescale' converts a palatal epithelium length (from ruha 8 to ruga 3) to a time (in embryonic days) using a series of best fit quadratic curves (see methods), for use in gemerating kymographs with function 'kymograph'

rescale<-function(l){
	
	w<-7*(10^-5)*l^2+0.0884*l+67.127
	
	t<-(-2)*(10^-5)*w^2+0.0187*w+10.858
	
	t
	
}


#Function 'reverse_rescale' converts a time (in embryonic days) to a palatal epithelium length (from ruga 8 to ruga 3) using the same calibration curves as in function 'rescale', for use in gemerating kymographs with function 'kymograph'

reverse_rescale<-function(t){

	aw<-(-2)*(10^-5)
	bw<-0.0187
	cw<-10.858
	
	al<-7*(10^-5)
	bl<-0.0884
	cl<-67.127
	
	###quadratic formula
	
	w<-(-bw+sqrt(bw^2-4*aw*(cw-t)))/(2*aw)
	
	l<-(-bl+sqrt(bl^2-4*al*(cl-w)))/(2*al)	
	
	l
	
}


#Function 'kymograph' produces a map of gene expression along the palatal epithelium over time
#Kymograph is for all specimens of specified type (E, M or T) and gene in 'input_profile'
#Time ranges between specified 'min' and 'max' in embryonic days
#Number of time points specified by 'steps'
#For each time point profile is a mean profile of all traces within a range specified by 'size', where a minumum of 'min' traces is required to return a profile (a maximum can also be specified with 'max')
#Profiles are aligned at ruga 8, which is identified by finding the first maximum in each profile - this is done comparing across a window of size specified by 'filter'

kymograph<-function(input_profile,type,gene,smooth_min=2,smooth_max=NA,min,max,size,steps,landmark=1000,length=2000,plot=TRUE,filter=100,...){
	
	m<-rep(NA,steps*length)
	M<-matrix(m,ncol=steps)
			
	#make list of profile lengths
	if(type=="T"){
		gene<-NA
	}
	if(!is.na(gene)){
		input_profile2<-input_profile[(input_profile[,1]==gene),]		
	}
	else{
		input_profile2<-input_profile
	}
	specimens_list<-unique(input_profile2[,2])
	num<-length(specimens_list)
	thickness<-expression_map(input_profile,"T",specimens=specimens_list,landmark=landmark,smooth=FALSE,plot=FALSE)
	if(type=="T"){
		expression<-thickness
	}
	else{
		expression<-expression_map(input_profile,type,gene,specimens=specimens_list,landmark=landmark,smooth=FALSE,plot=FALSE)
	}	
	len<-length(thickness[,1])
	upper<-len-filter
	s<-rep(NA,num*upper)
	S<-matrix(s,ncol=num)
	for(i in 1:upper){
		S[i,]<-(sign(thickness[i+filter,]-thickness[i,]))
	}
	K<-numeric(num)
	for(j in 1:num){
		i<-1
		X<-S[i,j]
		while(is.na(X)|(X>=1)){
			i<-i+1
			X<-S[i,j]
		}
		K[j]<-i
	}
	
	K<-landmark-K
	
	L<-seq(1:length(K))

	####rescale all length time proxies to time
	Kt<-rescale(K)
	
	####make time series and convert to lengths
	times<-seq(min,max,length=steps)
	L_times<-reverse_rescale(times)

	#use list of lengths (K) to select profiles in length range
	for(i in 1:steps){
		position<-times[i]###called position because originally a length not a time
		lower_bound<-position-size/2
		upper_bound<-position+size/2
		select<-L[(Kt>=lower_bound)&(Kt<=upper_bound)]
		length<-Kt[(Kt>=lower_bound)&(Kt<=upper_bound)]###called length because originally a length not a time
		distance<-position-length###called distance because originally a length not a time
		rank<-rank(abs(distance),ties.method="min")
		if(!is.na(smooth_max)){
			select<-select[rank<=smooth_max]			
		}
		if(length(select)>=smooth_min){
			select_profiles<-expression[,select]
			if(length(select)>1){
				M[,i]<-rowMeans(select_profiles,na.rm=TRUE)						
			}
			else{
				M[,i]<-select_profiles
			}
		}
	}

	R<-matrix(m,ncol=steps)
	for(i in 1:steps){
	
		rescale_len<-len-landmark+L_times[i]
		for(j in 1:rescale_len){
			R[j,i]<-M[(len-rescale_len+j),i]
			
		}
	}
	M<-R
	
	M<-t(M[,seq(length(M[1,]),1)])

	if(type=="T"){
		range<-grey.colors(100,1,0)
	}
	else{
		range<-grey.colors(100,0,1)
	}
	if(plot==TRUE){
		image(t(M),col=range,axes=FALSE)
	}
	M
}