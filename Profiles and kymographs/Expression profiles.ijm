//For an in situ hybridisation of a paraffin section through palate imaged with 20X objective
//Same specimen in same position taken with brightfield (ensuring correction for non-uniform illumination) and phase contrast, under 20X objective
//Images named with a set convention starting with the name of the gene (eg Shh), the specimen number (eg 1-23 4a) and the section number (eg 4) in the format "Shh 1-23 4a 4"
//Use macro "Directory" to set directory into which output text file will be written and set filename for output text file
//Use macros "Horizontal", "Vertical" and "Rotate" to orient image so that the posterior of the palate is to the left and the apical surface faces up
//Using the segmented line tool, trace the basal and apical surfaces of the palatal epithelium and log them using "Apical" or "Basal" macros (do this on the phase image where the bounds of the epithelium can be seen for unstained regions of tissue)
//Move the cursor to a consistent landmark found in all images (ruga 3) and log the position of this using the "Landmark" macro
//Subsequently, running the "Profile" macro on the brightfield image (rotated into the same orientation as surfaces were logged on the phase image) will convert the image to 8-bit and return a series of measurements
//1. The average epithelial staining intensity along a line perpendicular to the basal surface, bounded by the apical suface, at pixel increments along the basal surface
//2. The average mesenchymal staining intensity along a line perpendicular to the basal surface 50 pixels (which using the 20X objective was 25 µm) into the underlying mesenchyme, at pixel increments along the basal surface
//3. The distance between the apical and basal surfaces (in pixels) perpendicular to the basal surface at pixel increments along the basal surface
//4. The distance of the landmark (ruga 3) from the posterior of the palatal epithelium, measured relative to the basal surface
//For each successive image analysed, these measurements are appended to the output text file as three lines, one for the epithelial intensity, one for the mesenchymal intensity and one for the thickness
//Each line is of the form Gene name, Specimen number, Section number, Landmark position, Array type (epithelial (E), mesenchymal (M), thickness (T)), Array, separated by tabs, with each term in the arrays separated by spaces



function concatenate(array,sep){
	//given an array and a separator returns a string of array separated by separator (not 
	//my macro, found it online and copied it, but cannot remember where...)
	String.resetBuffer();
	String.append(array[0]);
	for(i=1;i<array.length;i++){
		String.append(sep);
		String.append(array[i]);
		}	
	str=String.buffer;
	return str;
	}


var bX;
var bY;
var aX;
var aY;
var lX;
var lY;
var dir;
var title;


macro "Directory [d]"{
	//selects which directory to write output text file into and a filename, and opens the file
	dir=getDirectory("Choose a Directory");
	title="Untitled";
	Dialog.create("Filename");
	Dialog.addString("Filename:", title);
	Dialog.show();
	title=Dialog.getString();
	}

macro "Horizontal [h]"{
	run("Flip Horizontally");  
	}

macro "Vertical [v]"{
	run("Flip Vertically");  
	}

macro "Rotate [r]"{
	run("Flip Horizontally"); 
	run("Flip Vertically");  
	}

macro "Basal [b]"{
	run("Fit Spline", "straighten"); 
	getSelectionCoordinates(bX, bY); 
	}

macro "Apical [a]"{
	run("Fit Spline", "straighten"); 
	getSelectionCoordinates(aX, aY); 
	}

macro "Landmark [l]"{
	getCursorLoc(lX,lY,z,f);
	}

macro "Profile [p]"{
	setBatchMode(true);

	image_name=getTitle();
	name_array=split(image_name," ");
	gene_name=name_array[0];
	specimen=name_array[1]+" "+name_array[2];
	slice_number=name_array[3];
	
	list=getFileList(dir);
	
	file_present=false;
	
	filename=title+".txt";
	
	for(i=0;i<list.length;i++){
		if(list[i]==filename){
			file_present=true;
			}
		}
	
	if(file_present==true){
		old_file=File.openAsString(dir+filename);
		f=File.open(dir+filename);
		
		old_arrays=split(old_file,"\n");
		for(i=0;i<old_arrays.length;i++){
			labels=split(old_arrays[i],"\t");
			if((gene_name!=labels[0])|(specimen!=labels[1])|(slice_number!=labels[2])){
				
				print(f,old_arrays[i]);

				}
			}				
		}
	
	else{
		f=File.open(dir+filename);
		}

	
	selectWindow(image_name);
	run("Duplicate...", "title=[for epithelium]");
	selectWindow("for epithelium");
	run("8-bit");
	selectWindow("for epithelium");
	run("Duplicate...", "title=[for mesenchyme]");
	selectWindow("for epithelium");
	
	aXr=newArray(aX.length);
	aYr=newArray(aY.length);
	for (i=0;i<aX.length;i++){
		aXr[aX.length-1-i]=aX[i];
		aYr[aX.length-1-i]=aY[i];
		}
	X=Array.concat(aXr,bX); 	
	Y=Array.concat(aYr,bY);
	makeSelection(3, X, Y);
	run("Make Inverse");
	setColor(0,0,0);
	run("Fill", "slice");
	run("Select None");
	
	
	epithelium=newArray(bX.length-10);
	thickness=newArray(bX.length-10);
	mesenchyme=newArray(bX.length-10);
	lm=newArray(bX.length-10);
	
	for (i=5; i<bX.length-5; i++) {	  	

	  	incX=bX[i+5]-bX[i-5];
		incY=bY[i+5]-bY[i-5];
		outX=incY/10;
		outY=-incX/10;

  		number=0;
  		sum=0;
		//first look epithelial side - measure 100 pixels as this is an excess so will certainly cover epithelium
  		selectWindow("for epithelium");
  		for (j=0; j<100; j++) {
  			intensity=getPixel(round(bX[i]+(j*outX)),round(bY[i]+(j*outY)));//round as otherwise seems to average intensity between neighbouring pixels sometimes being black therefore mixing foreground with background

  			sum=sum+intensity;
  			if(intensity>0){
  				number=number+1;
  				}  			

  			}
		
		mean=sum/number;

  		number2=0;
  		sum2=0;
		
		selectWindow("for mesenchyme");
		//second mesenchymal side - good 50 pixel deep as looks appropriate
		for (j=0; j<50; j++) {
  			intensity2=getPixel(round(bX[i]-(j*outX)),round(bY[i]-(j*outY)));
  			sum2=sum2+intensity2;
  			if(intensity2>0){
  				number2=number2+1;  			
  				}
  			}


  		mean2=sum2/number2;
		
		epithelium[i-5]=mean;
		thickness[i-5]=number;
  		mesenchyme[i-5]=mean2;
  		lm[i-5]=sqrt(pow((bX[i]-lX),2)+pow((bY[i]-lY),2));
  			
  		}

  	
  	Array.getStatistics(lm, min, max, mean, stdDev);

  	for (i=0; i<lm.length; i++) {
  		if(lm[i]==min){
  			reg=i;
  			}
  		}	  	

	epi=concatenate(epithelium," ");
	mes=concatenate(mesenchyme," ");
	thi=concatenate(thickness," ");
	
	print(f,gene_name+"\t"+specimen+"\t"+slice_number+"\t"+reg+"\tE\t"+epi);
	print(f,gene_name+"\t"+specimen+"\t"+slice_number+"\t"+reg+"\tM\t"+mes);
	print(f,gene_name+"\t"+specimen+"\t"+slice_number+"\t"+reg+"\tT\t"+thi);
	
  	selectWindow("for epithelium");
  	close();
  	selectWindow("for mesenchyme");
  	close(); 
	File.close(f);
	print(image_name+" logged");
	setBatchMode(false);
  	}
