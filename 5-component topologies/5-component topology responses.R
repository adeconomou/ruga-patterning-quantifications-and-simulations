#Script to identify response of all minial 5-component topologies (as identified in '5-component minimal topologies') to inhibition 
#To model the experimentally observed data, a system with three in-phase components (Wnt, Hh, mFGF) and two out-of-phase components (BMP, eFGF) is used
#The possible responses of a single component (Hh ie one of the three in-phase components) is in response to inhibition of itself and all other components is recorded for each topology
#Output a matrix where each row consists of a vector giving a topology response combination
#The first term in vector gives the topology number (position of topology in matrix Z from '5-component minimal topologies')
#Terms 2:26 give the matrix
#Terms 27:31 give whether a component is in the core (1) or not (0)
#Terms 32:36 give whether a component falls in a positive (1) or negative (-1) feedback loop (core or external)
#Terms 37:41 give whether the target component increases (1) or decreases (-1) in response to the inhibition of a component

library(igraph) #Version: 1.0.1	Depends: methods

#Function 'loops' to return all possible feedback loops in interaction matrix M
#First column gives number of components in loop
#Second column whether the loop is a positive or negative feedback loop
#Third column the sequence of the nodes through which the loop passes

loops<-function(M){
	
	l<-length(M[,1])

	L<-list()

	for(i in 1:l){
	
		N<-M
	
		N<-rbind(N,rep(0,l))
		N<-cbind(N,rep(0,(l+1)))
	
		N[(l+1),]<-N[i,]
		N[i,]<-rep(0,(l+1))
	
		g<-graph_from_adjacency_matrix(abs(t(N)))
		p<-all_simple_paths(g,i,(l+1))

		L<-c(L,p)

	}

	loops<-matrix(rep(0,(3*length(L))),ncol=3)

	for(j in 1:length(L)){
		
		###length
	
		c<-as.numeric(L[[j]])
	
		c[length(c)]<-c[1]
	
		loops[j,1]<-length(c)-1
	
		###sign
	
		sign<-1
	
		for(i in 1:(length(c)-1)){
		
			sign<-sign*M[c[(i+1)],c[i]]
	
		}
	
		loops[j,2]<-sign

		###loop

		lc<-length(c)-1

		i<-1

		stop<-F

		while(stop==F){
	
			if(T%in%(c==i)){
		
				stop<-T
			
				start<-(1:lc)[c[1:lc]==i]
			
				c2<-c(c[start:lc],c[1:(lc-1)])

			}
	
			i<-i+1
	
		}
	
		loops[j,3]<-sum(c2[1:lc]*10^((lc:1)-1))

	}

	loops<-unique(loops)

	loops<-loops[order(loops[,3]),]	

	loops<-loops[order(loops[,2]),]	

	output<-list(loops,M)
	
	output
	
}

#Function 'shorthand' converts a numeric vector where each term is an interger between 0 and 9 to a character string 

shorthand<-function(x){
	
	l<-length(x)
	
	sum(x[1:l]*10^((l:1)-1))
	
}

#Function 'longhand' is the reverse of function' shorthand'

longhand<-function(x){
	
	as.numeric(strsplit(as.character(x),split="")[[1]])
	
}


########

#two matrices Z and ZZ required

#ZZ is the outcome of the 5-component topology search ('5-component minimal topologies') before duplicated and non-minimal topologies are removed

#Z is the same as ZZ, except only core interactions are recorded (ie all interactions feeding into or out of external components are set to 0, as detailed in '5-component minimal topologies')

#cZ is number of comp in core
cZ<-sign(abs(Z[,1:5])+abs(Z[,6:10])+abs(Z[,11:15])+abs(Z[,16:20])+abs(Z[,21:25]))

#First set a 'target component' for which the responses to inhibition of all other components will be recorded
cT<-1 #target component (ie Hh readout) - could be any of 3 in phase
phase<-c(1,1,1,-1,-1)

count<-1

for(i in 1:length(Z[,1])){
	
	M<-matrix(as.numeric(Z[i,]),ncol=5,byrow=T)  #core only
	M1<-matrix(as.numeric(ZZ[i,]),ncol=5,byrow=T)  #all
	
	#identify core positive and negative feedback loops
	
	L<-loops(M) #as M is a minimal core, there will only be two loops recovered, one positive and one negative

	g<-graph_from_adjacency_matrix(abs(t(M)))
	
	A<-longhand(L[[1]][2,3])

	I<-longhand(L[[1]][1,3])
	
	AI<-unique(c(A,I))
	
	In<-setdiff(AI,A)
	
	#vector for which loop, whether core and direction of response of target for each compoent
	
	loop_temp<-numeric(5) #1 if positive, -1 if negative
	core_temp<-numeric(5) #will fill with 1 is core, 0 if not
	direction_temp<-numeric(5) #1 if target component increases on inhibition,-1 if decreases
	
	loop_temp[I]<--1
	loop_temp[A]<-1
	core_temp[AI]<-1	
	
	n<-1 #number of responses
	
	if(cT%in%AI){ #is target core
		
		#response of comp cT
	
		if((cT%in%A==T)&(cT%in%I==F)){ #only in positive feedback loop
			
			direction_temp[1]<--1
			
			
		}
		
		if((cT%in%A==F)&(cT%in%I==T)){ #only in negative feedback loop
			
			direction_temp[1]<-1
			
		}
		
		if((cT%in%A==T)&(cT%in%I==T)){ #in both loops so both responses possible (as inhibition at level of response for Hh)
			
			direction_temp<-rbind(direction_temp,direction_temp)
			
			direction_temp[1,1]<--1 #ie primarily through positive loop
			direction_temp[2,1]<-1 #ie primarily through negative loop
			
			n<-2
						
		}	
		
		AI2<-setdiff(AI,cT) #core components excluding cT
		
		for(j in 1:length(AI2)){ #effect of other core compoennts on cT
			
			p<-all_simple_paths(g,AI2[j],cT)
			
			###what is the sign of the net path? positive by default down, negative up
			
			p2<-as.numeric(p[[1]]) #may be 2 routes (one through pos and neg loop each), work out for first path, and second will be opposite response

			pi<-p2[-1]
			pj<-p2[-length(p2)]
			
			#'si' gives response dependent on sign of path from component to target
			
			if(length(pi)>1){
			
				si<--prod(diag(M[pi,pj])) #negative because if positive ie in phase, then decrease
				
			}else{
				
				si<--M[pi,pj]
				
			}
			
			#### reversed if positive loop excluded
			
			#'stab' gives stability of components excluded from path 
			
			stab<-1 #by default stays				
				
			if(length(setdiff(p2,A))==length(p2)){ #ie does the path (p2) exclude the activatory loop; if yes returns T, if no returns F
					
				stab<--1 #reversed if positive loop excluded
				
			}
			
			resp<-stab*si
								
			if(n==1){ #ie only one type of response
			
				direction_temp[AI2[j]]<-resp
			
			}else{
			
				if(length(p)==1){ #if two types of response for target to own inhibition, but only one type for component on target
				
					direction_temp[1,AI2[j]]<-resp
					direction_temp[2,AI2[j]]<-resp
										
				}else{ #ie more than one type of response, one path through positive feedback loop, one through negative
					
					#need to ensure matches properly with response of target to won inhibition
					if(In[1]%in%p2){ #ie inhibitory components therefore passes through negative feedback loop	
						
						direction_temp[1,AI2[j]]<-resp*-1
						direction_temp[2,AI2[j]]<-resp
						
					}else{
						
						direction_temp[1,AI2[j]]<-resp
						direction_temp[2,AI2[j]]<-resp*-1
						
					}
					
				}
													
			}
			
		}
		
		#effect of components external to core
		
		Mod<-setdiff(1:5,AI)
		
		if(length(Mod>0)){ #ie if there are modulators
			
			for(j in 1:length(Mod)){
				
				M2<-M1
				
				if(length(Mod)>1){ #ie if there are more than one mod
				
					M_remove<-setdiff(Mod,Mod[j])
				
					M2[M_remove,]<-M2[,M_remove]<-0 #ie remove columns relating to other mods so just mod of interest and core
				}	
				
				g2<-graph_from_adjacency_matrix(abs(t(M2)))
				
				p<-all_simple_paths(g2,Mod[j],cT)
					
				####taking same approach as above

				p2<-as.numeric(p[[1]]) #may be 2 routes (one through pos and neg loop each), work out for first path, and second will be opposite response

				pi<-p2[-1]
				pj<-p2[-length(p2)]
			
				if(length(pi)>1){
			
					si<--prod(diag(M2[pi,pj])) #negative because if positive ie in phase, then decrease
				
				}else{
				
					si<--M2[pi,pj]
				
				}
			
				#### reversed if positive loop excluded
			
				stab<-1 #by default stays				
				
				if(length(setdiff(p2,A))==length(p2)){ #ie does the path (p2) exclude the activatory loop; if yes returns T, if no returns F
					
					stab<--1 #reversed if positive loop excluded
				
				}

				resp<-stab*si
						
				if(n==1){ #ie only one type of response
			
					direction_temp[Mod[j]]<-resp
					
				}else{
			
					if(length(p)==1){
				
						direction_temp[1,Mod[j]]<-resp
						direction_temp[2,Mod[j]]<-resp
										
					}else{
					
						if(In[1]%in%p2){ #ie inhibitory components therefore passes through negative feedback loop						

							direction_temp[1,Mod[j]]<-resp*-1
							direction_temp[2,Mod[j]]<-resp
						
						}else{
						
							direction_temp[1,Mod[j]]<-resp
							direction_temp[2,Mod[j]]<-resp*-1
						
						}
					
					}
													
				}
					
				#as the sign of the path from an external component is not determined by phase, the response to inhibition also depends on the phase different between the two components
				
				ph<-phase[cT]/phase[Mod[j]] #positive if in phase, negative if out
			
				if(n==1){
				
					loop_temp[Mod[j]]<--direction_temp[Mod[j]]*ph #ie if out of phase and goes up, activatory etc
				
				}else{ #matrix rather than vector
					
					loop_temp[Mod[j]]<--direction_temp[1,Mod[j]]*ph #use first row where positive loop
					
				}
			
			}
			
		}
		
		##
	}else{  #ie if cT is an external component itself
		
		Mod<-setdiff(1:5,AI) #identify external componets
		
		### first how cT behaves
		
		M2<-M1
				
		if(length(Mod)>1){ #as before remove other mods for path anlysis
			
			M_remove<-setdiff(Mod,cT)
				
			M2[M_remove,]<-M2[,M_remove]<-0 #ie remove columns relating to other mods so just mod of interest and core
					
		}
		
		n<-1
		
		LL<-loops(M2)
		
		lM<-setdiff(LL[[1]][,3],L[[1]][,3]) #ie loop not in core
		
		if(length(lM)==1){ #it is possible for 2 loops, one through pos and one also through neg
			
			si<-LL[[1]][,2][LL[[1]][,3]==lM] ##sign of loop
		
			lM<-longhand(lM)
		
			stab<--1
		
			if(length(setdiff(lM,A))==length(A)){ ##ie does excluded include positive loop
			
				stab<-1
			
			}
		
			direction_temp[cT]<-stab*si #ie whether stable loop included and what sign of loops itself is
			
		}else{ #ie 2 loops
			
			direction_temp<-rbind(direction_temp,direction_temp)
			
			si<-LL[[1]][,2][LL[[1]][,3]==lM[1]] #does this loop, whatever sign, go through neg loop or not?
			
			lM<-longhand(lM[1])
			
			if(In[1]%in%lM){ # ie loop though negative loop, therefore want to reverse responses
				
				direction_temp[1,cT]<-si #eg negative loop (si=-1) going through negative feedback loop is pos through pos, therefore goes down
				direction_temp[2,cT]<--1*si #reverse
				
				loop_temp##################
				
			}else{
				
				direction_temp[1,cT]<--si
				direction_temp[2,cT]<-si
				
			}

			n<-2
		
		}
	
		if(n==1){
				
			loop_temp[cT]<--direction_temp[cT] #ie if goes down, positive loop
				
		}else{ #matrix rather than vector
					
			loop_temp[cT]<--direction_temp[1,cT] #use first row where positive loop
					
		}
	
		#### paths from core components to target component, again as above, more or less

		for(j in 1:length(AI)){
						
			g2<-graph_from_adjacency_matrix(abs(t(M2)))
				
			p<-all_simple_paths(g2,AI[j],cT)

			p2<-as.numeric(p[[1]]) #may be 2 routes (one through pos and neg loop each), work out for first path, and second will be opposite response

			pi<-p2[-1]
			pj<-p2[-length(p2)]
			
			if(length(pi)>1){
			
				si<--prod(diag(M2[pi,pj])) #negative because if positive ie in phase, then decrease
				
			}else{
				
				si<--M2[pi,pj]
				
			}
			
			#### reversed if positive loop excluded
			
			stab<-1 #by default stays				
				
			if(length(setdiff(p2,A))==length(p2)){ #ie does the path (p2) exclude the activatory loop; if yes returns T, if no returns F
					
				stab<--1 #reversed if positive loop excluded
				
			}
				
			#ph<-phase[cT]/phase[AI2[j]] #positive if in phase, negative if out
			
			resp<-stab*si
					
			if(length(p)==1){
		
				if(n==1){ #ie only one type of response
			
					direction_temp[AI[j]]<-resp
			
				}else{
				
					direction_temp[1,AI[j]]<-resp
					direction_temp[2,AI[j]]<-resp
								
				}
		
			}else{
			
				if(n==1){ #ie only one type of response
			
					direction_temp<-rbind(direction_temp, direction_temp)  ##so now have 2 entries
			
					n<-2
			
				}
			
				if(In[1]%in%p2){ #ie inhibitory components therefore passes through negative feedback loop						
				
					direction_temp[1,AI[j]]<-resp*-1
					direction_temp[2,AI[j]]<-resp
						
				}else{
						
					direction_temp[1,AI[j]]<-resp
					direction_temp[2,AI[j]]<-resp*-1
				
				}
		
			}
		
		}
				
		###effect of other mods
	
		if(length(Mod)>1){ #ie more than jsu cT as mod
	
			Mod2<-M_remove #list of other mods from above
			
			for(j in 1:length(Mod2)){
				
				M2<-M1
				
				if(length(Mod2)>1){ #as before remove other mods for path anlysis
					
					M_remove<-setdiff(Mod2,Mod2[j])
					
					M2[M_remove,]<-M2[,M_remove]<-0 #ie remove columns relating to other mods so just mod of interest and core
					
				}
					
				#### again, as above
				
				g2<-graph_from_adjacency_matrix(abs(t(M2)))
				
				p<-all_simple_paths(g2,Mod2[j],cT)

				p2<-as.numeric(p[[1]]) #may be 2 routes (one through pos and neg loop each), work out for first path, and second will be opposite response

				pi<-p2[-1]
				pj<-p2[-length(p2)]
			
				if(length(pi)>1){
			
					si<--prod(diag(M2[pi,pj])) #negative because if positive ie in phase, then decrease
				
				}else{
				
					si<--M2[pi,pj]
				
				}
			
				#### reversed if positive loop excluded
			
				stab<-1 #by default stays				
				
				if(length(setdiff(p2,A))==length(p2)){ #ie does the path (p2) exclude the activatory loop; if yes returns T, if no returns F
					
					stab<--1 #reversed if positive loop excluded
				
				}

				resp<-stab*si
						
				if(length(p)==1){
		
					if(n==1){ #ie only one type of response
			
						direction_temp[Mod2[j]]<-resp
			
					}else{
					
						direction_temp[1,Mod2[j]]<-resp
						direction_temp[2,Mod2[j]]<-resp
								
					}
		
				}else{
			
					if(n==1){ #ie only one type of response
			
						direction_temp<-rbind(direction_temp, direction_temp)  ##so now have 2 entries
						
						n<-2
						
					}
			
					if(In[1]%in%p2){ #ie inhibitory components therefore passes through negative feedback loop						
				
						direction_temp[1,Mod2[j]]<-resp*-1
						direction_temp[2,Mod2[j]]<-resp
						
					}else{
						
						direction_temp[1,Mod2[j]]<-resp
						direction_temp[2,Mod2[j]]<-resp*-1
				
					}
		
				}
					
				ph<-phase[cT]/phase[Mod2[j]] #positive if in phase, negative if out
			
				if(n==1){
				
					loop_temp[Mod2[j]]<--direction_temp[Mod2[j]]*ph #ie if out of phase and goes up, activatory etc
				
				}else{ #matrix rather than vector
					
					loop_temp[Mod2[j]]<--direction_temp[1,Mod2[j]]*ph #use first row where positive loop
					
				}	
				
			}		
		
		}	
		
	}
	
	#combine into a vector topology number, topology, core and loop identity	
	out_temp<-c(i,as.numeric(ZZ[i,]),core_temp,loop_temp)
	
	#add response type to output vector
	if(n==1){
		
		out_temp<-c(out_temp,direction_temp)
		
		out_temp<-t(as.matrix(out_temp))
		
	}else{
		
		out_temp<-rbind(out_temp,out_temp)
		
		out_temp<-cbind(out_temp,direction_temp)
		
	}
		
	write.table(out_temp,append=T,"~/Documents/outputs/responses output.txt",sep="\t",col.names=F,row.names=F)
		
	if(count==100*round(count/100)){
	
		print(count)	
	
	}
	
	count<-count+1
	
}

##########
##########

#As in '5-component minimal topologies' need to remove non-minimal topologies found in Z/ZZ

#Read in table from above
rt<-as.matrix(read.table("~/Desktop/outputs/responses output.txt",sep="\t"))

X<-rt[,2:26] #note, this is longer than ZZ above as many topologies can produce different behaviours and so are duplicated

#remove duplicates by same method as in '5-component minimal topologies'

W<-unique(X)

#remove topologies derived from others

l<-length(W[,1])

r<-numeric()

#to avoid removing topologies with different variants of a topology reverse each number group

sW<-rowSums(abs(W))

#order by length of topology

oW<-order(sW)

osW<-sW[oW]

WW<-W[oW,]

rev<-numeric()

for(i in min(osW):max(osW)){
	
	ran<-range((1:l)[osW==i])

	rev<-c(rev,(ran[2]:ran[1]))
	
}

for(i in 1:25){

	print(i)
	
	rf<-(1:l)[duplicated(WW[,-i])]
		
	rrev<-rev[duplicated(WW[rev,-i])]

	rtot<-c(rf,rrev)

	rr<-rtot[duplicated(rtot)]

	r<-c(r,rr)
	
}

r<-unique(r)

V<-WW[-r,]


###############

## remove topologies that are not minimal (although at first allow repeated minimals)

v<-apply((V+1),1,toString)
x<-apply((X+1),1,toString)

k<-x%in%v

##topologies with core and loop identity, and responses

output<-unique(rt[k,]) #all topologies with their responses
